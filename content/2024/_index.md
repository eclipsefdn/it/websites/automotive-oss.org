---
title: Automotive Open Source Summit 2024
seo_title: Automotive Open Source Summit 2024
date: 2024-01-29T10:00:00-04:00
headline: 'Join the Automotive Open Source Summit 2024'
custom_jumbotron: |
    <div class="automotive-oss-jumbotron-2022">
        <div class="jumbotron-head">
            <div class="jumbotron-title-container">
                <p class="jumbotron-subtitle">
                  Advancing Open Source Innovation in Automotive Grade Software
                </p>
                <p>
                  May 14, 2024<br>
                  Hotel Vier Jahreszeiten | Starnberg, Germany
                </p>
                <p class="text-primary featured-jumbotron-opening-soon">
                  Registration Opening Soon
                </p>
            </div>
        </div>
    </div>
header_wrapper_class: "header-automotive-oss-event-2023"
summary: 'The Automotive Open Source Summit is an annual gathering of senior architects, innovators, and executives from around the world. At this event, attendees come together to discuss the latest trends in automotive grade open source software, network with like-minded professionals, and learn about the various technologies and challenges faced in automotive software development. As the inaugural event, the 2023 Automotive Open Source Summit will be focused around topics for senior architects and executives, with the outlook of expanding the conversation towards developers and engineers in the future. The Automotive Open Source Summit is a great place for automotive professionals to gain knowledge, keep up to date with the latest developments, and make valuable connections in the industry.'
categories: []
keywords: ["Open Source Automotive Software","Open Source Automotive Technology","Automotive Software Conference","Automotive Software Development","Vehicle Software Engineering","Automotive Software","Automotive Technology","Automotive Software Communities"]
slug: ""
aliases: []
hide_breadcrumb: true
container: container-fluid
layout: single 
cascade:
    header_wrapper_class: header-automotive-oss-event-2023
    hide_sidebar: true
    hide_page_title: true
    page_css_file: /public/css/2024.css
---

<!-- About and Registration -->
{{< grid/section-container isMarkdown="true" class="featured-section-row featured-section-row-lighter-bg row white-row text-center padding-y-60" >}}

## What to Expect

The Automotive Open Source Summit stands as the foremost open source event
for the automotive sector, dedicated to education, collaboration, and
heightened awareness, while actively advocating for the widespread adoption of
open source methodologies and best practices in the automotive industry. With a
code-first and community-driven approach, this summit serves as a catalyst for
fostering innovation.

Participants engage in meaningful discussions on the latest advancements in
automotive-grade open source software, establish connections with like-minded
professionals, and gain valuable insights into the evolving technologies and
challenges in automotive software development.

Ready to be a part of shaping the future of software-connected vehicles? We
invite you to join us – just let us know you're in. There's a small fee of 100
EUR that will be applied once your attendance request is approved.

{{</ grid/section-container >}}

<!-- Key Topics -->
{{< grid/div id="key-topics" isMarkdown="false" class="container padding-top-60 padding-bottom-60 text-center" >}}
    {{< grid/div isMarkdown="false" class="row" >}}
        <h2>Key Topics</h2>
        <figure class="col-sm-8">
            <img class="img-responsive" src="images/key-topics/open-source-challenges.jpg" alt="">
            <figcaption class="h3">
              Open Source Cultural and Organisational Challenges
            </figcaption>
        </figure>
        <figure class="col-sm-8">
            <img class="img-responsive" src="images/key-topics/role.jpg" alt="">
            <figcaption class="h3">
              The Role of Open Source in Automotive Software
            </figcaption>
        </figure>
        <figure class="col-sm-8">
            <img class="img-responsive" src="images/key-topics/stack.jpg" alt="">
            <figcaption class="h3">
              The Automotive-Grade Open Source Software Stack
            </figcaption>
        </figure>
    {{</ grid/div >}}
{{</ grid/div >}}

<!-- Past Speakers -->
{{< grid/section-container class="speakers-section featured-section-row-dark-bg padding-top-40 padding-bottom-40 text-center" id="speakers" >}}
    {{< events/user_display title="Past Speakers" event="default" year="2023" source="speakers" imageRoot="/2023/images/speakers/" >}}
    {{</ events/user_display >}}
{{</ grid/section-container >}}

<!-- Additional Info -->
{{< grid/section-container id="additional-info" class="featured-section-row featured-section-row-lighter-bg white-row padding-top-60 padding-bottom-60 text-center featured-story" isMarkdown="false" >}}
    <h2>Lodging & Travel</h2>
    <div class="row">
        <div class="col-md-10 col-md-offset-2">
            <h3>Lodging</h3>
            <p>
              We've secured a group of rooms at discounted rates for conference
              participants at two hotels:
            </p>
            <ul>
              <li>
                Vier Jahreszeiten Starnberg. To obtain more details or make a
                reservation, kindly contact
                <a href="mailto:info@vier-jahreszeiten-starnberg.de">info@vier-jahreszeiten-starnberg.de</a>.
              </li>
              <li>
                Hotel FischerHaus Starnberg. For additional information or to
                book, please contact 
                <a href="mailto:info@hotel-fischerhaus-starnberg.de">info@hotel-fischerhaus-starnberg.de</a>. 
              </li>
            </ul>
        </div>
        <div class="col-md-10">
            <h3>Additional Activities</h3>
            <p>
              We kindly ask you to join us the day before the event for some
              extracurricular community outdoor activities. More information to
              follow. 
            </p>
        </div>
    </div>
{{</ grid/section-container >}}

<!-- Re-live AOSS -->

{{< grid/section-container class="featured-section-row featured-section-row-lighter-bg text-center padding-y-60" >}}
  <h2 class="margin-bottom-40">Highlights from AOSS 2023</h2>
  <div class="col-sm-10 col-sm-offset-7">
    <h3>Re-live AOSS 2023</h3>
    {{< youtube "lxi0K2nQsV8" "max" >}}
  </div>
  <div class="col-xs-24">
    <h3>Our Eclipse SDV Community in Action</h3>
    <a class="btn btn-primary" href="https://www.youtube.com/@EclipseSDV/playlists">
      Subscribe to our Channel
    </a>
  </div>
{{</ grid/section-container >}}

<!-- Agenda -->

{{< grid/section-container class="featured-section-row featured-section-row-lighter-bg padding-y-60" >}}
  <h2 id="agenda">Agenda at a Glance</h2>
  <table>
    <thead>
      <tr>
        <th>Type</th>
        <th>Session</th>
        <th>Scheduled Time*</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td aria-label="Type not applicable"></td>
        <td>Check-in & Registration</td>
        <td>8:00 - 9:00</td>
      </tr>
      <tr>
        <td>Keynote</td>
        <td>Welcoming Words</td>
        <td>9:00 - 9:15</td>
      </tr>
      <tr>
        <td>Keynote</td>
        <td>Keynotes / Panel / Discussion</td>
        <td>9:15 - 10:00</td>
      </tr>
      <tr>
        <td>Break</td>
        <td>Coffee Break</td>
        <td>10:00 - 10:30</td>
      </tr>
      <tr>
        <td>Keynote</td>
        <td>Keynotes / Panel / Discussion</td>
        <td>10:30 - 12:00</td>
      </tr>
      <tr>
        <td>Break</td>
        <td>Lunch</td>
        <td>12:00 - 13:30</td>
      </tr>
      <tr>
        <td>Keynote</td>
        <td>Keynotes / Panel / Discussion</td>
        <td>13:30 - 15:30</td>
      </tr>
      <tr>
        <td>Break</td>
        <td>Coffee Break</td>
        <td>15:30 - 16:00</td>
      </tr>
      <tr>
        <td>Keynote</td>
        <td>Keynotes / Panel / Discussion</td>
        <td>16:00 - 17:30</td>
      </tr>
      <tr>
        <td>Keynote</td>
        <td>Closing Words</td>
        <td>17:30 - 17:45</td>
      </tr>
      <tr>
        <td>Break</td>
        <td>Networking & Cocktails</td>
        <td>18:00 - onwards</td>
      </tr>
    </tbody>
  </table>
{{</ grid/section-container >}}

<!-- Organizers -->
{{< grid/section-container id="organizers" class="text-center featured-story">}}
  {{< events/sponsors event="default" year="2023" source="coorganizers" title="Co-organizers" useMax="false" displayBecomeSponsor="false" headerClass="padding-bottom-0" itemClass="padding-0" >}}
{{</ grid/section-container >}}
