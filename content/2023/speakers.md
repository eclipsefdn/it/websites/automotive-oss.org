---
title: Speakers 
description: Speakers for Automotive OSS Summit 2023
hide_page_title: false
---

{{< events/user_bios event="default" year="2023" source="speakers" imgRoot="../images/speakers/" >}}
