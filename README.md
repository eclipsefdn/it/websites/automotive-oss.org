# automotive-oss.org

The [automotive-oss.org](https://automotive-oss.org) website is generated with [Hugo](https://gohugo.io/documentation/).

The Automotive Open Source Summit is an annual gathering of senior architects, innovators, and executives from around the world. At this event, attendees come together to discuss the latest trends in automotive grade open source software, network with like-minded professionals, and learn about the various technologies and challenges faced in automotive software development. As the inaugural event, the 2023 Automotive Open Source Summit will be focused around topics for senior architects and executives, with the outlook of expanding the conversation towards developers and engineers in the future. The Automotive Open Source Summit is a great place for automotive professionals to gain knowledge, keep up to date with the latest developments, and make valuable connections in the industry.

## Getting started

### Required Software

| Software  | Version   |
|---        |---        |
| node.js   | 18.13.0   |
| npm       | 8.19      |
| Hugo      | 0.110     |
| Git       | > 2.31    |

See our [Managing Required Software](https://gitlab.eclipse.org/eclipsefdn/it/webdev/hugo-solstice-theme/-/wikis/Managing-Required-Software) 
wiki page for more information on this topic. Install dependencies, build
assets and start a webserver:

```bash
yarn
hugo server
```

## Contributing

1. [Fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) the [automotive-oss.org](https://gitlab.eclipse.org/eclipsefdn/it/websites/automotive-oss.org) repository
2. Clone repository: `git clone https://gitlab.eclipse.org/[your_eclipsefdn_username]/automotive-oss.org.git`
3. Create your feature branch: `git checkout -b my-new-feature`
4. Commit your changes: `git commit -m 'Add some feature' -s`
5. Push feature branch: `git push origin my-new-feature`
6. Submit a merge request

### Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License v. 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0

## Related projects

### [EclipseFdn/solstice-assets](https://gitlab.eclipse.org/eclipsefdn/it/webdev/solstice-assets)

Images, less and JavaScript files for the Eclipse Foundation look and feel.

### [EclipseFdn/hugo-solstice-theme](https://gitlab.eclipse.org/eclipsefdn/it/webdev/hugo-solstice-theme/)

Hugo theme of the Eclipse Foundation look and feel.

## Bugs and feature requests

Have a bug or a feature request? Please search for existing and closed issues. If your problem or idea is not addressed yet, [please open a new issue](https://gitlab.eclipse.org/eclipsefdn/it/websites/automotive-oss.org/-/issues).


## Author

**Christopher Guindon (Eclipse Foundation)**

- <https://twitter.com/chrisguindon>
- <https://github.com/chrisguindon>

## Trademarks

* Eclipse® is a Trademark of the Eclipse Foundation, Inc.

## Copyright and license

Copyright 2023 the [Eclipse Foundation, Inc.](https://www.eclipse.org) and the [automotive-oss.org authors](https://gitlab.eclipse.org/eclipsefdn/it/websites/automotive-oss.org/-/graphs/main). Code released under the [Eclipse Public License Version 2.0 (EPL-2.0)](https://gitlab.eclipse.org/eclipsefdn/it/websites/automotive-oss.org/-/raw/main/README.md).
